import argparse
from subprocess import call
import os

FNULL = open(os.devnull, 'w')

parser=argparse.ArgumentParser();
parser.add_argument('-case', default='cantilever', help='cantilever or LBracket; physical case and number of elements')
parser.add_argument('-symmetry', type=int, default=1, help="1 if symmetry and 0 else")
parser.add_argument('-ini', '--initialization', nargs='+', default=['classic'], help='classic for from scratch; file if from solution + name of the file')

parser.add_argument('-lV', type=float, default=1., help='coef for volume - unchanged')
parser.add_argument('-lCply', type=float, default=0.01, help='coef for compliance - 2000 for the cantilever and 0.01 for the LBracket')
parser.add_argument('-lplast', type=float, default=10000, help="multiplier for the plasticity")
parser.add_argument('-muplast', type=float, default=0, help="penalized in the Augmented Lagrangian for the plasticity")
parser.add_argument('-threshold', type=float, default=0)
parser.add_argument('-pnorm', type=int, default=8)

parser.add_argument('-coefMax', nargs=3, default=[5,1.2,0.6], help="coef max, first coef = by what we multiply if OK and second coef = by what we multiply if NOT OK")
parser.add_argument('-tol', nargs=3, default=[2,0.9,100], help='tol initial, rate and frequency')

parser.add_argument('-NIT', type=int, default=500, help='number of iteration')
parser.add_argument('-computer', type=str, default="mac", help='mac or lab')
parser.add_argument('-noMeshCreate', action='store_true', help='if mesh already exists')
parser.add_argument('-debug', type=int, default=0, help='debug')
parser.add_argument('-freqPlotGraph', type=int, default=30, help='debug')
parser.add_argument('-plot', action='store_true', help='plot')

args = parser.parse_args()


pathResults = "results"
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/" + args.case + "_sym" + str(args.symmetry)
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
if args.initialization[0] == 'classic':
	pathResults = pathResults + "/classic"
else:
	pathResults = pathResults + "/"+str(args.initialization[1])
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/lV"+str(args.lV)+"_lCply"+str(args.lCply)+'_lplast'+str(args.lplast)+'_muplast'+str(args.muplast)+"_thres"+str(args.threshold)+'_pnorm'+str(args.pnorm)
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/tolINI" + str(args.tol[0]) + "_rateTol" + str(args.tol[1]) + "_freqTol" + str(args.tol[2])
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/NIT" + str(args.NIT) + "_coefIni" + str(args.coefMax[0]) + "_rateCoefOK" + str(args.coefMax[1]) + "_rateCoefNO" + str(args.coefMax[2]);
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathPhi = pathResults+"/phi"
call(["mkdir",pathPhi], stdout=FNULL, stderr=FNULL)
pathVM = pathResults+"/VonMises"
call(["mkdir",pathVM], stdout=FNULL, stderr=FNULL)

pathAnnex = "Annex"
call(["mkdir",pathAnnex], stdout=FNULL, stderr=FNULL)

if os.listdir(pathAnnex) == []:
    numLastFile=0
else:
	listerep=[f for f in os.listdir(pathAnnex) if f[0] != '.']
	listerep.sort()
	lastFile = listerep[-1]
	numLastFile = int(lastFile)
	numLastFile = numLastFile+1
pathAnnex = pathAnnex + "/" + str(numLastFile)
call(["mkdir",pathAnnex], stdout=FNULL, stderr=FNULL)

toWrite = 'string pathResults = "' + pathResults + '";'+"\n"
toWrite = toWrite + 'string pathAnnex = "'+ pathAnnex + '";'+"\n"
toWrite = toWrite +'string case = "'+args.case+'";'+"\n"
toWrite = toWrite + 'real lengthSide = '+str(1)+';'+"\n"
toWrite = toWrite + 'string initialization = "'+args.initialization[0]+'";'+'\n'
toWrite = toWrite + 'int symmetry = '+str(args.symmetry)+';'+'\n'
toWrite = toWrite + "int kx, ky; string file; "
if args.case == 'cantilever':
	toWrite = toWrite + 'int n = 4;'+"\n"
	toWrite =  toWrite + "real coefAlpha = "+str(1)+";"+'\n'
	if args.symmetry == 1:
		toWrite =  toWrite + 'include "../configuration_files/meshes_2D/cantilever_mesh_shortTop.edp"'+'\n'
	else:
		toWrite =  toWrite + 'include "../configuration_files/meshes_2D/cantilever_mesh.edp"'+'\n'		
	toWrite = toWrite + "real Eyoung, poisson, mu, lambda, gx, gy, yieldStress;"+"\n"
	toWrite = toWrite + "Eyoung = 1960; poisson = 0.30; yieldStress = 0.95; gx = 0; gy = -1.1;" + "\n"
	toWrite = toWrite + 'func phi0=-cos((kx+1)/4*pi*x)*cos((ky+1)/4*pi*y)+0.65-1.0;'+'\n'
	ys = 0.95
	if args.initialization[0] == 'classic':
		toWrite = toWrite + "kx = "+str(10)+"; ky = "+str(13)+";"+"\n"
	else:
		toWrite = toWrite + 'file = "'+args.initialization[1]+'";'+"\n"
elif args.case == 'LBracket':
	toWrite = toWrite + 'int n = 3;'+"\n"
	toWrite =  toWrite + "real coefAlpha = "+str(5)+";"+'\n'
	toWrite =  toWrite + 'include "../configuration_files/meshes_2D/L_mesh.edp"'+'\n'
	toWrite = toWrite + 'func phi0 = cos((kx+1)/4*pi*(x-0.05))*cos((ky+1)/4*pi*(y-0.5))+0.8-1.0;'+'\n'
	toWrite = toWrite + "real Eyoung, poisson, mu, lambda, gx, gy, yieldStress;"+"\n"
	toWrite = toWrite + "Eyoung = 1; poisson = 0.3; yieldStress = 20; gx = 0; gy = -10;" + "\n"
	ys = 20	
	if args.initialization[0] == 'classic':
		toWrite = toWrite + "kx = "+str(10)+"; ky = "+str(13)+";"+"\n"
	else:
		toWrite = toWrite + 'file = "'+args.initialization[1]+'";'+"\n"

toWrite = toWrite + "\n"

toWrite = toWrite + "real lagV = "+str(args.lV)+";"+"\n"
toWrite = toWrite + "real lagCply = "+str(args.lCply)+";"+"\n"
toWrite = toWrite + "real lPlast = "+str(args.lplast)+";"+"\n"
toWrite = toWrite + "real muPlast = "+str(args.muplast)+";"+"\n"
toWrite = toWrite + "real threshold = "+str(args.threshold)+";"+"\n"
toWrite = toWrite + "int pnorm = "+str(args.pnorm)+";"+"\n"

toWrite =  toWrite + "real tol = "+str(args.tol[0])+"; real rateTol = "+str(args.tol[1])+"; int freqTol = "+str(args.tol[2])+";"+"\n"
toWrite =  toWrite + "real coefIni = "+str(args.coefMax[0])+"; real rateCoefYES = "+str(args.coefMax[1])+"; real rateCoefNO = "+str(args.coefMax[2])+";"+"\n"
toWrite = toWrite + "real coef = coefIni;" + "\n"
toWrite =  toWrite + "int NIT = "+str(args.NIT)+";"+"\n"

if args.computer == "mac":
	toWrite =  toWrite + 'string pathAdvect= "/Users/mathilde/bin/advect";'
else:
	toWrite =  toWrite + 'string pathAdvect= "/home/mathilde.boissier/bin/advect";'

if args.noMeshCreate is False:
	toWrite =  toWrite + "int meshCreate = "+str(1)+";"
else:
	toWrite =  toWrite + "int meshCreate = "+str(0)+";"

toWrite =  toWrite + "int debug = "+str(args.debug)+";"+"\n"
toWrite =  toWrite + "int freqPlotGraph = "+str(args.freqPlotGraph)+";"+"\n"

toWrite = toWrite + "\n"

file = open("mainEx.edp", "r")
total = toWrite + file.read()
file.close()

file = open("main.edp","w")
file.write(total)
file.close()

call(["cp","main.edp",pathResults+"/main.edp"], stdout=FNULL, stderr=FNULL)

file = open(pathResults+"/plot.edp", "w")
toWrite = 'load "medit" //pour freefem'+'\n'
toWrite = toWrite + 'load "iovtk" //pour paraview'+'\n'
toWrite = toWrite + 'load "distance"'+'\n'
toWrite = toWrite + 'int[int] fforder = [1];'+'\n'
toWrite = toWrite + 'real[int] visoX = [-100, -0.001, 0.001, 100];'+'\n'
toWrite = toWrite + 'mesh Th = readmesh("ThPlot.mesh");'+'\n'

toWrite = toWrite + 'real sigmaY = '+str(ys)+';'+'\n'

toWrite = toWrite + 'fespace Vh1(Th,P1); fespace Vh0(Th,P0);'+'\n'
toWrite = toWrite + 'int np=Th.nv; int nt=Th.nt; //np = # nodes, nt = # elements'+'\n'
toWrite = toWrite + 'real[int] loadsol(np); string dummy, svol;'+'\n'

toWrite = toWrite + '/* Shape initialization (phi, distance, X) */'+'\n'
toWrite = toWrite + '		Vh1 phi,X;'+'\n'
toWrite = toWrite + '		Vh1 VMsigma;'+'\n'
toWrite = toWrite + 'string savingName;'+'\n'
toWrite = toWrite + 'real eps1 = 0.001;'+'\n'
toWrite = toWrite + 'for (int it = 0; it<=500; it = it+1){'+'\n'

toWrite = toWrite + '	if (it<10){savingName = "00" + it + ".sol";}'+'\n'
toWrite = toWrite + '	if (9<it && it<100){savingName = "0" + it + ".sol";}'+'\n'
toWrite = toWrite + '	if (99<it){savingName = it + ".sol";}'+'\n'

toWrite = toWrite + '	{ifstream f("phi/"+savingName);'+'\n'
toWrite = toWrite + '		for(int i=0; i<8; i++) {'+'\n'
toWrite = toWrite + '			f>>dummy;'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '		for(int i=0; i<np; i++) {'+'\n'
toWrite = toWrite + '				f>>loadsol[i];'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '	phi[] = loadsol;}'+'\n'
toWrite = toWrite + '	X = eps1+(1-eps1)*(phi<0);'+'\n'

toWrite = toWrite + '	plot(X, value=1, fill=1, WindowIndex = 1, cmm = it);'+'\n'

toWrite = toWrite + '	{ifstream f("VonMises/"+savingName);'+'\n'
toWrite = toWrite + '		for(int i=0; i<8; i++) {'+'\n'
toWrite = toWrite + '			f>>dummy;'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '		for(int i=0; i<np; i++) {'+'\n'
toWrite = toWrite + '				f>>loadsol[i];'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '	VMsigma[] = loadsol;}'+'\n'
toWrite = toWrite + '	VMsigma = VMsigma/(sigmaY^2.);'

toWrite = toWrite + '	plot(VMsigma, value=1, fill=1, WindowIndex = 2, cmm = it);'+'\n'

toWrite = toWrite + 'if(it == 0){'+'\n'
toWrite = toWrite +	'	savevtk("E0.vtk", Th, phi, order=fforder);'+'\n'
toWrite = toWrite +	'	savevtk("VM0.vtk", Th, VMsigma, order=fforder);'+'\n'
toWrite = toWrite +	'}'+'\n'
toWrite = toWrite + '\n'
toWrite = toWrite +	'if(it == 300){'+'\n'
toWrite = toWrite +	'	savevtk("E300.vtk", Th, phi, order=fforder);'+'\n'
toWrite = toWrite +	'	savevtk("VM300.vtk", Th, VMsigma, order=fforder);'+'\n'
toWrite = toWrite +	'}'+'\n'

toWrite = toWrite + '}'+'\n'
file.write(toWrite)
file.write("\n")
file.close()

if args.plot is False:
	call(["FreeFem++","main.edp","-nw","-v",str(args.debug)])
else:
	call(["FreeFem++","main.edp","-v",str(args.debug)])
