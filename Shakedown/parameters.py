import argparse
from subprocess import call
import os

FNULL = open(os.devnull, 'w')

parser=argparse.ArgumentParser();
parser.add_argument('-case', default='cantilever', help='cantilever or LBracket; physical case and number of elements')
parser.add_argument('-symmetry', type=int, default=1, help="1 if symmetry and 0 else")
parser.add_argument('-ini', '--initialization', nargs='+', default=['classic', 11, 11], help='classic for from scratch + number of holes in x and y; file if from solution + name of the file')
parser.add_argument('-iniRho', type=int, default=0, help='0: [0,0,0,0]; 1: Ae(u)')

parser.add_argument('-lV', type=float, default=1., help='coef for compliance')
parser.add_argument('-lCply', type=float, default=2000, help='coef for compliance')
parser.add_argument('-l0', type=float, default=0)
parser.add_argument('-mu0', type=float, default=100)
parser.add_argument('-lM', type=float, default=0)
parser.add_argument('-muM', type=float, default=100)
parser.add_argument('-threshold', type=float, default=0)

parser.add_argument('-coefMax', nargs=3, default=[5,1.2,0.6], help="coef max, first coef = by what we multiply if OK and second coef = by what we multiply if NOT OK")
parser.add_argument('-tol', nargs=3, default=[1.6,0.9,100], help='tol initial, rate and frequency')
parser.add_argument('-NIT', type=int, default=600, help='number of iteration')

parser.add_argument('-l0rho', type=float, default=1)
parser.add_argument('-lMrho', type=float, default=1)
parser.add_argument('-coefMaxRho', nargs=3, default=[1,1.3,0.4], help="coef max, first coef = by what we multiply if OK and second coef = by what we multiply if NOT OK")
parser.add_argument('-NITRho', type=int, default=50, help='number of iteration')

parser.add_argument('-computer', type=str, default="mac", help='mac or lab')
parser.add_argument('-noMeshCreate', action='store_true', help='if mesh already exists')

parser.add_argument('-debug', type=int, default=0, help='debug')
parser.add_argument('-freqPlotGraph', type=int, default=30, help='debug')
parser.add_argument('-plot', action='store_true', help='plot')

args = parser.parse_args()


pathResults = "results"
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/" + args.case + "_sym" + str(args.symmetry)
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
if args.initialization[0] == 'classic':
	pathResults = pathResults + "/classic"+"_iniRho"+str(args.iniRho)
else:
	pathResults = pathResults + "/file_" +str(args.initialization[1])+"_iniRho"+str(args.iniRho)
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/lV"+str(args.lV)+"_lCply"+str(args.lCply)+"_l0"+str(args.l0)+'_mu0'+str(args.mu0)+'_lM'+str(args.lM)+'_muM'+str(args.muM)+"_thres"+str(args.threshold)+"_l0r"+str(args.l0rho)+"_lMr"+str(args.lMrho)
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/tolINI" + str(args.tol[0]) + "_rateTol" + str(args.tol[1]) + "_freqTol" + str(args.tol[2])
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathResults = pathResults + "/NIT" + str(args.NIT) + "_coefIni" + str(args.coefMax[0]) + "_rateCoefOK" + str(args.coefMax[1]) + "_rateCoefNO" + str(args.coefMax[2])+"_NITRho"+str(args.NITRho);
call(["mkdir",pathResults], stdout=FNULL, stderr=FNULL)
pathPhi = pathResults+"/phi"
call(["mkdir",pathPhi], stdout=FNULL, stderr=FNULL)
pathVM = pathResults+"/VM0"
call(["mkdir",pathVM], stdout=FNULL, stderr=FNULL)
pathVM1 = pathResults+"/VMM"
call(["mkdir",pathVM1], stdout=FNULL, stderr=FNULL)

pathAnnex = "Annex"
call(["mkdir",pathAnnex], stdout=FNULL, stderr=FNULL)

if os.listdir(pathAnnex) == []:
    numLastFile=0
else:
    lastFile = os.listdir(pathAnnex)[-1]
    numLastFile = int(lastFile)
    numLastFile = numLastFile+1
pathAnnex = pathAnnex + "/" + str(numLastFile)
call(["mkdir",pathAnnex], stdout=FNULL, stderr=FNULL)

if args.initialization[0] == 'file':
	call(["cp",args.initialization[1],pathResults+"/ini.sol"], stdout=FNULL, stderr=FNULL)


toWrite = 'string pathResults = "' + pathResults + '";'+"\n"
toWrite = toWrite + 'string pathAnnex = "'+ pathAnnex + '";'+"\n"
toWrite = toWrite +'string case = "'+args.case+'";'+"\n"
toWrite = toWrite + 'real lengthSide = '+str(1)+';'+"\n"
toWrite = toWrite + 'string initialization = "'+args.initialization[0]+'";'+'\n'
toWrite = toWrite + 'int symmetry = '+str(args.symmetry)+';'+'\n'
toWrite = toWrite + "int kx, ky; string file; "
toWrite = toWrite + "int iniRho = "+str(args.iniRho)+";"+'\n'
if args.initialization[0] == 'classic':
	toWrite = toWrite + "kx = "+str(args.initialization[1])+"; ky = "+str(args.initialization[2])+";"+"\n"
else:
	toWrite = toWrite + 'file = "'+args.initialization[1]+'";'+"\n"
if args.case == 'cantilever':
	toWrite = toWrite + 'int n = 4;'+"\n"
	toWrite = toWrite + "int pnorm = 8;"+"\n"
	toWrite =  toWrite + "real coefAlpha = "+str(1)+";"+'\n'
	if args.symmetry == 1:
		toWrite =  toWrite + 'include "../configuration_files/meshes_2D/cantilever_mesh_shortTop.edp"'+'\n'
	else:
		toWrite =  toWrite + 'include "../configuration_files/meshes_2D/cantilever_mesh.edp"'+'\n'		
	toWrite = toWrite + "real Eyoung, poisson, mu, lambda, gx, gy, yieldStress;"+"\n"
	toWrite = toWrite + "Eyoung = 1960; poisson = 0.30; yieldStress = 0.95; gx = 0; gy = -1.1;" + "\n"
	toWrite = toWrite + 'func phi0=-cos((kx+1)/4*pi*x)*cos((ky+1)/4*pi*y)+0.65-1.0;'+'\n'
	ys = 0.95
	if args.initialization[0] == 'classic':
		toWrite = toWrite + "kx = "+str(10)+"; ky = "+str(13)+";"+"\n"
	else:
		toWrite = toWrite + 'file = "'+args.initialization[1]+'";'+"\n"
elif args.case == 'LBracket':
	toWrite = toWrite + 'int n = 3;'+"\n"
	toWrite = toWrite + "int pnorm = 16;"+"\n"
	toWrite =  toWrite + "real coefAlpha = "+str(5)+";"+'\n'
	toWrite =  toWrite + 'include "../configuration_files/meshes_2D/L_mesh.edp"'+'\n'
	toWrite = toWrite + 'func phi0 = cos((kx+1)/4*pi*(x-0.05))*cos((ky+1)/4*pi*(y-0.5))+0.8-1.0;'+'\n'
	toWrite = toWrite + "real Eyoung, poisson, mu, lambda, gx, gy, yieldStress;"+"\n"
	toWrite = toWrite + "Eyoung = 1; poisson = 0.3; yieldStress = 20; gx = 0; gy = -10;" + "\n"
	ys = 20	
	if args.initialization[0] == 'classic':
		toWrite = toWrite + "kx = "+str(10)+"; ky = "+str(13)+";"+"\n"
	else:
		toWrite = toWrite + 'file = "'+args.initialization[1]+'";'+"\n"

toWrite = toWrite + "\n"

toWrite = toWrite + "real lagV = "+str(args.lV)+";"+"\n"
toWrite = toWrite + "real lagCply = "+str(args.lCply)+";"+"\n"
toWrite = toWrite + "real l0 = "+str(args.l0)+";"+"\n"
toWrite = toWrite + "real mu0 = "+str(args.mu0)+";"+"\n"
toWrite = toWrite + "real lM = "+str(args.lM)+";"+"\n"
toWrite = toWrite + "real muM = "+str(args.muM)+";"+"\n"
toWrite = toWrite + "real threshold = "+str(args.threshold)+";"+"\n"

toWrite =  toWrite + "real tol = "+str(args.tol[0])+"; real rateTol = "+str(args.tol[1])+"; int freqTol = "+str(args.tol[2])+";"+"\n"
toWrite =  toWrite + "real coefIni = "+str(args.coefMax[0])+"; real rateCoefYES = "+str(args.coefMax[1])+"; real rateCoefNO = "+str(args.coefMax[2])+";"+"\n"
toWrite = toWrite + "real coef = coefIni;" + "\n"

toWrite =  toWrite + "int NIT = "+str(args.NIT)+";"+"\n"

toWrite = toWrite + "real l0rho = "+str(args.l0rho)+";"+"\n"
toWrite = toWrite + "real lMrho = "+str(args.lMrho)+";"+"\n"

toWrite =  toWrite + "real coefIniRho = "+str(args.coefMaxRho[0])+"; real rateCoefRhoYES = "+str(args.coefMaxRho[1])+"; real rateCoefRhoNO = "+str(args.coefMaxRho[2])+";"+"\n"
toWrite = toWrite + "real coefRho = coefIniRho;" + "\n"

toWrite =  toWrite + "int NITRho = "+str(args.NITRho)+";"+"\n"

if args.computer == "mac":
	toWrite =  toWrite + 'string pathAdvect= "/Users/mathilde/bin/advect";'
else:
	toWrite =  toWrite + 'string pathAdvect= "/home/mathilde.boissier/bin/advect";'

if args.noMeshCreate is False:
	toWrite =  toWrite + "int meshCreate = "+str(1)+";"
else:
	toWrite =  toWrite + "int meshCreate = "+str(0)+";"

toWrite =  toWrite + "int debug = "+str(args.debug)+";"+"\n"
toWrite =  toWrite + "int freqPlotGraph = "+str(args.freqPlotGraph)+";"+"\n"

toWrite = toWrite + "\n"

file = open("mainEx.edp", "r")
total = toWrite + file.read()
file.close()

file = open("main.edp","w")
file.write(total)
file.close()

call(["cp","main.edp",pathResults+"/main.edp"], stdout=FNULL, stderr=FNULL)

file = open(pathResults+"/plot.edp", "w")
toWrite = 'load "medit" //pour freefem'+'\n'
toWrite = toWrite + 'load "iovtk" //pour paraview'+'\n'
toWrite = toWrite + 'load "distance"'+'\n'
toWrote = toWrite + 'int[int] fforder = 1;'+'\n'

toWrite = toWrite + 'real[int] visoX = [-100, -0.001, 0.001, 100];'+'\n'
toWrite = toWrite + 'mesh Th = readmesh("ThPlot.mesh");'+'\n'

toWrite = toWrite + 'real sigmaY = '+str(ys)+';'+'\n'

toWrite = toWrite + 'fespace Vh1(Th,P1); fespace Vh0(Th,P0);'+'\n'
toWrite = toWrite + 'int np=Th.nv; int nt=Th.nt; //np = # nodes, nt = # elements'+'\n'
toWrite = toWrite + 'real[int] loadsol(np); string dummy, svol;'+'\n'

toWrite = toWrite + '/* Shape initialization (phi, distance, X) */'+'\n'
toWrite = toWrite + '		Vh1 phi,X;'+'\n'
toWrite = toWrite + '		Vh1 VM0, VMM;'+'\n'
toWrite = toWrite + 'string savingName;'+'\n'
toWrite = toWrite + 'real eps1 = 0.001;'+'\n'
toWrite = toWrite + 'for (int it = 0; it<=500; it = it+1){'+'\n'

toWrite = toWrite + '	if (it<10){savingName = "00" + it + ".sol";}'+'\n'
toWrite = toWrite + '	if (9<it && it<100){savingName = "0" + it + ".sol";}'+'\n'
toWrite = toWrite + '	if (99<it){savingName = it + ".sol";}'+'\n'

toWrite = toWrite + '	{ifstream f("phi/"+savingName);'+'\n'
toWrite = toWrite + '		for(int i=0; i<8; i++) {'+'\n'
toWrite = toWrite + '			f>>dummy;'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '		for(int i=0; i<np; i++) {'+'\n'
toWrite = toWrite + '				f>>loadsol[i];'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '	phi[] = loadsol;}'+'\n'
toWrite = toWrite + '	X = eps1+(1-eps1)*(phi<0);'+'\n'

toWrite = toWrite + '	plot(X, value=1, fill=1, WindowIndex = 1, cmm = it);'+'\n'

toWrite = toWrite + '	{ifstream f("VM0/"+savingName);'+'\n'
toWrite = toWrite + '		for(int i=0; i<8; i++) {'+'\n'
toWrite = toWrite + '			f>>dummy;'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '		for(int i=0; i<np; i++) {'+'\n'
toWrite = toWrite + '				f>>loadsol[i];'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '	VM0[] = loadsol;}'+'\n'
toWrite = toWrite + '	VM0 = VM0/(sigmaY^2.);'+'\n'
toWrite = toWrite + '	plot(VM0, value=1, fill=1, WindowIndex = 2, cmm = "VM0 "+it);'+'\n'

toWrite = toWrite + '	{ifstream f("VMM/"+savingName);'+'\n'
toWrite = toWrite + '		for(int i=0; i<8; i++) {'+'\n'
toWrite = toWrite + '			f>>dummy;'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '		for(int i=0; i<np; i++) {'+'\n'
toWrite = toWrite + '				f>>loadsol[i];'+'\n'
toWrite = toWrite + '		}'+'\n'
toWrite = toWrite + '	VMM[] = loadsol;}'+'\n'
toWrite = toWrite + '	VMM = VMM/(sigmaY^2.);'+'\n'
toWrite = toWrite + '	plot(VMM, value=1, fill=1, WindowIndex = 2, cmm = "VMM "+it);'+'\n'

toWrite = toWrite + 'if(it == 0){'+'\n'
toWrite = toWrite +	'	savevtk("E0.vtk", Th, phi, order=fforder);'+'\n'
toWrite = toWrite +	'	savevtk("VM0-0.vtk", Th, VM0, order=fforder);'+'\n'
toWrite = toWrite +	'	savevtk("VMM-0.vtk", Th, VMM-0, order=fforder);'+'\n'
toWrite = toWrite +	'}'+'\n'
toWrite = toWrite + '\n'
toWrite = toWrite +	'if(it == '+str(args.NIT)+'){'+'\n'
toWrite = toWrite +	'	savevtk("E300.vtk", Th, phi, order=fforder);'+'\n'
toWrite = toWrite +	'	savevtk("VM0-300.vtk", Th, VM0, order=fforder);'+'\n'
toWrite = toWrite +	'	savevtk("VMM-300.vtk", Th, VMM, order=fforder);'+'\n'
toWrite = toWrite +	'}'+'\n'

toWrite = toWrite + '}'+'\n'
file.write(toWrite)
file.write("\n")
file.close()

if args.plot is False:
	call(["FreeFem++","main.edp","-nw","-v",str(args.debug)])
else:
	call(["FreeFem++","main.edp","-v",str(args.debug)])
